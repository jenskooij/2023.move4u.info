require('dotenv').config();
const contentful = require('contentful');

module.exports = new Promise((resolve, reject) => {
    const client = contentful.createClient({
        space: process.env.CONTENTFULL_SPACE,
        environment: process.env.CONTENTFULL_ENVIRONMENT, // defaults to 'master' if not set
        accessToken: process.env.CONTENTFULL_ACCESS_TOKEN
    })

    client.getEntries({
        content_type: process.env.THEME_PAGE_CONTENT_TYPE_ID,
        'include': 3
    })
        .then(entry => {
            resolve(entry);
        })
        .catch(reject);
});
