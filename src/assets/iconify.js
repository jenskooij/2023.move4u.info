(() => {
  window.contentfulApp.init(function (sdk) {
    sdk.window.startAutoResizer();
    console.log('init');
    document.getElementById('searchField').focus();

    const searchBox = document.getElementById('searchBox');
    const resultsBox = document.getElementById('resultsBox');
    const results = document.getElementById('results');

    window.addEventListener("message", (event) => {
      const data = event.data;
      console.log('received message:', event);

      if (data.method === 'connect') {
        console.log('connected to contentful');
        const currentValue = data.params[0].field.value;
        console.log('current value:', currentValue);
        if (currentValue !== undefined) {
          console.log('Initial value already there, so show that instead');
          searchBox.close();
          const item = document.createElement('img');
          item.setAttribute('src', currentValue);
          item.setAttribute('width', `64`);
          item.setAttribute('height', `64`);
          results.appendChild(item);
          resultsBox.show();
        }
      }

    }, false);

    document.getElementById('searchAgain').addEventListener('click', () => {
      resultsBox.close();
      searchBox.show();
    });

    function imageClick (e) {
      sdk.field.setValue(e.currentTarget.getAttribute('src'));
      results.innerHTML = '';
      results.appendChild(e.currentTarget);
      e.currentTarget.removeEventListener('click', imageClick);
      e.currentTarget.setAttribute('width', `64`);
      e.currentTarget.setAttribute('height', `64`);
      e.currentTarget.setAttribute('style', '');
    }

    document.getElementById('searchForm').addEventListener('submit', (e) => {
      const query = document.querySelector('[name=query]').value;
      resultsBox.close();
      results.innerHTML = '';
      fetch(`https://api.iconify.design/search?query=${query}&limit=999`).then(response => response.json()).then((data) => {
        data.icons.forEach(el => {
          const item = document.createElement('img');
          let parts = el.split(':');
          item.setAttribute('src', `https://api.iconify.design/${parts[0]}/${parts[1]}.svg`);
          item.setAttribute('width', `32`);
          item.setAttribute('height', `32`);
          item.setAttribute('style', 'cursor:pointer;');
          item.addEventListener('click', imageClick);
          results.appendChild(item);
        });
        resultsBox.show();
      });
    });
  });



})();