const axios = require("axios"),
  env = require('dotenv'),
  qs = require('querystring'),
  sgMail = require('@sendgrid/mail');;

exports.handler = function (event, context, callback) {
  function failureResponse () {
    callback(null, {
      statusCode: 500,
      headers: {
        "Access-Control-Allow-Origin": "*", // Allow from anywhere
        "Access-Control-Allow-Headers": "*", // Allow all headers
      },
      body: `<div class="notification is-error">
  Er is iets fout gegaan met je inschrijving. Probeer het later opnieuw of mail naar ledenadministratie@move4u.info
</div>`
    });
  }

  function successResponse () {
    callback(null, {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin": "*", // Allow from anywhere
        "Access-Control-Allow-Headers": "*", // Allow all headers
      },
      body: `
<div class="notification is-success">
  Bedankt voor je inschrijving. De ledenadministratie gaat je aanmelding verwerken.
</div>`
    });
  }

  function successResponseOptions () {
    callback(null, {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin": "*", // Allow from anywhere
        "Access-Control-Allow-Headers": "*", // Allow all headers
      }
    });
  }

  function convertDataToTable (fields) {
    let html = '<table>';
    for (const key in fields) {
      if (key === 'g-recaptcha-response') {
        continue;
      }
      html += `
      <tr>
        <th>${key.replaceAll('_', ' ')}</th>
        <td>${fields[key]}</td>
      </tr>
      `;
    }
    html += '</table>';
    return html;
  }

  function sendMail (htmlTable) {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: 'jenskooij1988@gmail.com',
      from: 'website@move4u.info', // Use the email address or domain you verified above
      subject: 'Nieuwe aanmelding Move4u via de website',
      html: `<p>Er heeft een nieuwe aanmelding plaatsgevonden via de website. Deze persoon heeft de volgende gegevens achtergelaten.</p>` + htmlTable,
    };
    sgMail
      .send(msg)
      .then(() => {
        successResponse();
      }, error => {
        console.error(error);

        if (error.response) {
          console.error(error.response.body)
        }
        failureResponse();
      });
  }

  if (event.httpMethod === 'OPTIONS') {
    successResponseOptions();
  } else if (event.httpMethod === 'POST') {
    const secret = process.env.GOOGLE_SITE_SECRET,
      fields = qs.parse(event.body),
      response = fields['g-recaptcha-response'];

    axios.post(`https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${response}`)
      .then(response => {
        if (response.data.success) {
          if (response.data.score >= 0.5) {
            // Convert data to table
            const htmlTable = convertDataToTable(fields);
            // Send mail
            sendMail(htmlTable);
          } else {
            failureResponse();
          }
        } else {
          console.log(response.data);
          failureResponse();
        }
      }).catch(error => {
      console.log('Google Captch Response failed: ', error);
      failureResponse();
    });
  } else {
    callback(null, {
      statusCode: 400
    });
  }
};