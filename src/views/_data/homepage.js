require('dotenv').config();
const contentful = require('contentful');

module.exports = new Promise((resolve, reject) => {
    const client = contentful.createClient({
        space: process.env.CONTENTFULL_SPACE,
        environment: process.env.CONTENTFULL_ENVIRONMENT, // defaults to 'master' if not set
        accessToken: process.env.CONTENTFULL_ACCESS_TOKEN
    })

    client.getEntries({
        content_type: process.env.HOMEPAGE_CONTENT_TYPE_ID,
        'sys.id': process.env.HOMEPAGE_ENTRY_ID,
        'include': 2
    }).then((response) => {
        resolve(response.items[0])
    })
        .catch(reject);
});