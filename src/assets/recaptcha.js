function onSubmit () {
  const event = new Event('verified');
  const elem = document.querySelector("#inschrijfformulier");
  if (elem.checkValidity()) {
    elem.dispatchEvent(event);
  } else {
    elem.reportValidity();
  }
}