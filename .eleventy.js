const htmlmin = require("html-minifier");

module.exports = function (eleventyConfig) {
  /**
   * The file extensions Eleventy should pick-up and render
   */
  eleventyConfig.setTemplateFormats([
    "njk",
    "md"
  ]);

  /**
   * Everyting that gets rendered as .html file, will get html minified
   */
  eleventyConfig.addTransform("htmlmin", function (content, outputPath) {
    if (outputPath.endsWith(".html")) {
      return htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      });
    }

    return content;
  });

  eleventyConfig.addShortcode("link", function(linkComponent) {
    if (!linkComponent || !linkComponent.sys.contentType) {
      console.warn('Calling link shortcode without linkComponent. It is likely that this has to do with the include depth: ', linkComponent);
      return `/entry/${linkComponent.sys.id}`;
    }
    if (linkComponent.sys.contentType.sys.id === process.env.TEAM_PAGE_CONTENT_TYPE_ID) {
      return `/teams/${linkComponent.fields.slug}`;
    } else if (linkComponent.sys.contentType.sys.id === process.env.HOMEPAGE_CONTENT_TYPE_ID) {
      return '/';
    } else if (linkComponent.sys.contentType.sys.id === undefined) {
      return '/404';
    } else {
      return `/${linkComponent.fields.slug}`;
    }
  });

  /**
   * Copy assets to dist root
   */
  eleventyConfig.addPassthroughCopy({ "src/assets": "../site/" });

  /**
   * Set Eleventy input and output dirs
   * @type {{output: string, input: string}}
   */
  eleventyConfig.dir = {
    input: "src/views",
    output: "dist/site"
  };

  return eleventyConfig;
};