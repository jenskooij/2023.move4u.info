require('dotenv').config();

module.exports = {
  url: process.env.URL || "http://localhost:8080",
  lambdaUrl: (process.env.URL ? process.env.URL + '/.netlify/functions' : 'http://localhost:9000/.netlify/functions'),
  googleSiteKey: process.env.GOOGLE_SITE_KEY,
  siteName: "Volleybalvereniging Move4u",
  siteDescription:
    "Description",
  authorName: "Jens Kooij",
  twitterUsername: "jenskooij", // no `@`
  socialImage: "img/favicons/social.png",
  faviconPng: "img/favicons/favicon.png",
  faviconSvg: "img/favicons/favicon.svg",
  faviconIco: "img/favicons/favicon.ico",
  tileColor: "#fff",
  themeColor: "#ff7f04"
};