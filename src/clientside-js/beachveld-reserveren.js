(() => {
  const widgets = document.querySelectorAll('.js-beachveld-reserveren'),
    reserveButton = document.querySelectorAll('.js-beachveld-reserveren-button');
  let reservations = [];
  if (widgets.length > 0) {

    /**
     * Reserveringen ophalen
     */
    fetch('https://move4ubeach.jnsmvc.nl/beschikbaarheid/api?date=' + (new Date()).toISOString().substr(0,10)).then(response => response.json()).then((data) => {
      for (let hour in data) {
        if (Object.prototype.hasOwnProperty.call(data, hour)) {
          const entry = data[hour];
          if (entry.reservedFields > 0) {
            let t = document.createElement('template');
            t.innerHTML = `<div class="box has-background-secondary">
                  <div>
                      <strong class="has-text-grey-dark">${hour}:00 (${entry.reservedFields} ${entry.reservedFields > 1 ? "velden" : "veld"} gereserveerd)</strong>
                  </div>
                  <div>${entry.by.join(",")}</div>
              </div>`
            reservations.push(t.content);
          }
        }
      }
      widgets.forEach(widget => {
        widget.querySelector('.progress').classList.add('is-hidden');
        widget.parentNode.parentNode.querySelector('input[type="date"]').value = (new Date()).toISOString().substr(0,10);
        reservations.forEach(reservation => {
          widget.appendChild(reservation);
        });
        if (reservations.length === 0) {
          let t = document.createElement('template');
          t.innerHTML = `
<div class="box has-background-grey-light mt-3">
  <div>
    <em>Geen reserveringen vandaag</em>
  </div>
</div>`;
          widget.appendChild(t.content);
        }
      });
    })
  }

  function clickReserveringButton(e) {
    e.currentTarget.parentNode.parentNode.querySelector('.js-beachveld-overview').classList.add('is-hidden');
    e.currentTarget.parentNode.parentNode.querySelector('.is-hidden:not(.js-beachveld-overview):not(.progress)').classList.remove('is-hidden');
    e.currentTarget.parentNode.removeChild(e.currentTarget);
  }

  reserveButton.forEach(button => {
    button.addEventListener('click', clickReserveringButton);
  });
})();