const richTextTypes = require("@contentful/rich-text-types");
module.exports = {
  renderNode: {
    [richTextTypes.INLINES.ENTRY_HYPERLINK]: (node, children) => {
      if (node.data.target.sys.contentType.sys.id === process.env.HOMEPAGE_CONTENT_TYPE_ID) {
        return `<a href="/" title="${node.data.target.fields.title}">${node.content[0].value}</a>`;
      } else if (node.data.target.sys.contentType.sys.id === process.env.TEAM_PAGE_CONTENT_TYPE_ID) {
        return `<a href="/teams/${node.data.target.fields.slug}" title="${node.data.target.fields.title}">${node.content[0].value}</a>`;
      }
      return `<a href="/${node.data.target.fields.slug}" title="${node.data.target.fields.title}">${node.content[0].value}</a>`;
    },
    [richTextTypes.BLOCKS.TABLE]: (node, children) => {
      return `<div class="table-container">
  <table class="table table-layout-fixed">
    ${children(node.content)}
  </table>
</div>`;
    },
    [richTextTypes.INLINES.EMBEDDED_ENTRY]: (node, children) => {
      console.log("embedded entry, wont render", node);
    },
    [richTextTypes.INLINES.HYPERLINK]: (node, children) => {
      return `<a href="${node.data.uri}" target="_blank">${node.content[0].value}</a>`;
    },
    [richTextTypes.INLINES.ASSET_HYPERLINK]: (node, children) => {
      return `<a href="${node.data.target.fields.file.url}" title="${node.data.target.fields.file.fileName}" target="_blank">${node.content[0].value}</a>`;
    },
    [richTextTypes.BLOCKS.EMBEDDED_ASSET]: (node, children) => {
      if (node.data.target.fields) {
        if (node.data.target.fields.file.contentType.includes("image")) {
          return `<img data-large="${node.data.target.fields.file.url}" src="${node.data.target.fields.file.url}?fm=webp&amp;w=960&amp;h=540" alt="${node.data.target.fields.title}" title="${node.data.target.fields.title}" />`
        } else {
          return `<a href="${node.data.target.fields.file.url}" class="button" target="_blank" title="${node.data.target.fields.file.fileName}">
  <img class="image is-24x24 mr-3 is-inline-block" src="https://api.iconify.design/material-symbols/download.svg"/>
  <span>${node.data.target.fields.title === "" ? node.data.target.fields.file.fileName : node.data.target.fields.title}</span>
  </a>`;
        }
      } else {
        console.log(`Ignore invalid asset in rich text; skipping asset width id ${node.data.target.sys.id}`);
      }
    }
  }
}