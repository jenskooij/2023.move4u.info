require('dotenv').config();
const contentful = require('contentful');
const documentToHtmlString = require('@contentful/rich-text-html-renderer');
const richTextTypes = require("@contentful/rich-text-types");
const renderOptions = require('./contentfulRenderOptions');

module.exports = function () {
    return new Promise((resolve, reject) => {
        const client = contentful.createClient({
            space: process.env.CONTENTFULL_SPACE,
            environment: process.env.CONTENTFULL_ENVIRONMENT, // defaults to 'master' if not set
            accessToken: process.env.CONTENTFULL_ACCESS_TOKEN
        })

        client.getEntries({
            content_type: process.env.TEAM_PAGE_CONTENT_TYPE_ID
        })
            .then(entries => {
                entries.items.forEach(entry => {
                    entry.contentHTML = documentToHtmlString.documentToHtmlString(entry.fields.content, renderOptions);
                });

                function compareTeamNaam(a, b) {
                    if (a.fields.teamNaam < b.fields.teamNaam) {
                        return -1;
                    }
                    if (a.fields.teamNaam > b.fields.teamNaam) {
                        return 1;
                    }
                    return 0;
                }

                entries.items = entries.items.sort(compareTeamNaam);

                resolve(entries);
            })
            .catch(reject);
    });
}
