(() => {
  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        entry.target.classList.add('animated');
      } else {
        if (!entry.target.classList.contains('animate-once')) {
          entry.target.classList.remove('animated');
        }
      }
    })
  })

  const animateOnScrollElements = document.querySelectorAll('.animate-on-scroll');
  animateOnScrollElements.forEach((el) => {
    observer.observe(el);
  })
})();